Counter = new Meteor.Collection('counts');
Deps.autorun(function () {
	Meteor.subscribe("usercollection");
  if(Meteor.userId()){
	  Meteor.subscribe("counter",Meteor.userId());
  }
});

Handlebars.registerHelper('ucfirst', function(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
});

Template.online_users.userStatus = function() {
	return Meteor.users.find();
};
Template.user_loggedout.events({
	"click #login": function(e, tmpl) {
		Meteor.loginWithFacebook({
		}, function(err) {
			if (err) {
        console.log(err);
			} else {
			}
		});
	}
});

Template.user_loggedin.helpers({
  score: function () {
    Meteor.call("score",function(error, data) {
		return data;
	});
  }
});

Template.user_loggedin.events({
	"click #logout": function(e, tmpl) {
		Meteor.logout(function(err) {
			if (err) {
			} else {
			}
		});
	}
});


Template.clock.clock = function(){
	return Counter.findOne(Meteor.userId());
};

Template.game.events({
	"click img": function(e, tmpl) {
		Meteor.call("choice", e.currentTarget.id, function(error, data) {
			if(data) {
				$('.choosen').removeClass('active');
				$('#'+data).addClass('active');
			}
		});
	}
});