Accounts.onCreateUser(function(options, user) {
	if (options.profile) {
		options.profile.picture = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/";
		user.profile = options.profile;
	}
	return user;
});

var game;

Meteor.startup(function() {
	game = new Game();
	game.init();
});

Meteor.publish("usercollection", function() {
	return Meteor.users.find({"profile.online": true}, {fields: {emails: 1, profile: 1, score: 1, lastUsed: 1}});
});

Meteor.publish("counter", function(userId) {
	var self = this;
	var initializing = true;
	var handle = game.bind('changed', function() {
		self.changed("counts", userId, {count: game.Counter, choosed: game.Users[userId]});
	});
	var initializing = false;
	self.added("counts", userId, {count: game.Counter, choosed: game.Users[userId]});
	self.ready();
	self.onStop(function() {
	});
});

Meteor.methods({
	choice: function(id) {
		return game.addChoice(id,Meteor.userId());;
	},
	score: function() {
		return game.getScore(Meteor.userId());;
	}
});

Meteor.setInterval(function() {
	game.countdown(Meteor.users);
}, 1000);