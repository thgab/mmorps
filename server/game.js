Game = function () {
  _.extend(this, {
	  Choice : {},
	  Process : {},
	  UsersChoices : {},
	  Users : {},
	  ValidValues : ['rock','paper','scissors'],
	  Bet : 10,
	  StartValue : 1000,
	  Counter : 10,
	  CounterLock : false,
  });
};
_.extend(Game.prototype, Backbone.Events);
_.extend(Game.prototype, {
	init: function(){
		this.Counter = 10;
		this.Choice = {};
		this.Process = {};
		this.UsersChoices = {};
		for(var i=0;i<this.ValidValues.length;i++){
			this.UsersChoices[this.ValidValues[i]] = [];
		}
	},
	addChoice: function (choice, userId) {
		if(_.indexOf(this.ValidValues, choice)>=0){
			this.Choice[userId] = choice;
			for(var del in this.Users){
				this.UsersChoices[del] = _.without(this.UsersChoices[del],userId);
			}
			this.UsersChoices[choice].push(userId);
		}
		return choice;
	},
	processing: function(usercollection){
		for(var i=0;i<this.ValidValues.length;i++){
			var currentindex = i;
			var loserindex = ((i-1)+3)%3;
			for(var w=0;w<this.UsersChoices[this.ValidValues[currentindex]].length;w++){
				for(var l=0;l<this.UsersChoices[this.ValidValues[loserindex]].length;l++){
					this.win(this.UsersChoices[this.ValidValues[currentindex]][w], this.UsersChoices[this.ValidValues[loserindex]][l]);
				}
			}
		}
		for(var user in this.Users){
			usercollection.upsert(user, { $set: { score: this.Users[user] , lastUsed: this.Choice[user] }});
		}
		this.init();
	},
	win: function(winnerId,userId){
		if (typeof this.Users[winnerId] === 'undefined') {
			this.Users[winnerId] = this.StartValue;
		}
		if (typeof this.Users[userId] === 'undefined') {
			this.Users[userId] = this.StartValue;
		}
		this.Users[winnerId] += this.Bet;
		this.Users[userId] -= this.Bet;
	},
	getScore: function(userId){
		if (typeof this.Users[userId] === 'undefined') {
			this.Users[userId] = this.StartValue;
		}
		return this.Users[userId];
	},
	countdown: function(usercollection){
		if(this.CounterLock){
			return;
		}
		this.Counter--;
		this.trigger('changed');
		if(this.Counter == 0){
			this.CounterLock = true;
			this.processing(usercollection);
			this.CounterLock = false;
		}
	}
});
