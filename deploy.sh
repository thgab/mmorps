#!/bin/sh
af login
OUTPUT_DIR=~/tmp/demeteorized
APPNAME=mmorps
rm -rf $OUTPUT_DIR
demeteorizer -n v0.10.11 -o $OUTPUT_DIR
cd $OUTPUT_DIR
npm install
npm shrinkwrap
af update $APPNAME